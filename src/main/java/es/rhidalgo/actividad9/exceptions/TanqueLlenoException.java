package es.rhidalgo.actividad9.exceptions;

public class TanqueLlenoException extends Exception {
    public TanqueLlenoException(int carga) {
        super("No se pueden añadir " + carga
                + " litros. El tanque se desbordaría");
    }
}
