package es.rhidalgo.actividad9.exceptions;

public class TanqueVacioException extends Throwable {

    public TanqueVacioException(String mensaje) {

        super(mensaje);
    }
}
