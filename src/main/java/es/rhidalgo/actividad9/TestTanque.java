package es.rhidalgo.actividad9;

import es.rhidalgo.actividad9.exceptions.TanqueLlenoException;
import es.rhidalgo.actividad9.exceptions.TanqueVacioException;

public class TestTanque {
    public static void main(String[] args) {

        Tanque tanque = new Tanque(10);

        try {
            tanque.anyadir(5);
            tanque.retirar(3);
            tanque.anyadir(100);
        } catch (TanqueLlenoException ex) {
            System.out.println(ex.getMessage());
        } catch (TanqueVacioException ex) {
            System.out.println(ex.getMessage());
        }
    }
}
