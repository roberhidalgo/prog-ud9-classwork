package es.rhidalgo.actividad9;

import es.rhidalgo.actividad9.exceptions.TanqueLlenoException;
import es.rhidalgo.actividad9.exceptions.TanqueVacioException;

public class Tanque {

    private int capacidad; // Capacidad del tanque
    private int carga;

    public Tanque(int capacidad) {
        this.capacidad = capacidad;
        this.carga = 0;
    }

    public void anyadir(int carga) throws TanqueLlenoException {

        if (carga < 0) {
            return;
        }

        this.carga += carga;

        if (this.carga > capacidad) {
            this.carga -= carga;
            throw new TanqueLlenoException(carga);
        }

        System.out.println("Carga añadida con éxito");
    }

    public void retirar(int carga) throws TanqueVacioException {

        if (carga < 0) {
            return;
        }

        this.carga -= carga;

        if (this.carga < 0) {
            this.carga += carga;
            throw new TanqueVacioException("No se pueden retirar " + carga + " litros. No hay suficiente carga");
        }

        System.out.println("Carga retirada con éxito");
    }
}
