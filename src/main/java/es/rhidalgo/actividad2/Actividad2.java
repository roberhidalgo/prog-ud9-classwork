package es.rhidalgo.actividad2;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad2 {

    private static final int NUM_LEIDOS = 6;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int i = 0;
        int maximo = Integer.MIN_VALUE;

        do {
            try {
                int numero = leerNumeroEntero(scanner);
                if (numero > maximo) {
                    maximo = numero;
                }
                i++;
            }catch (InputMismatchException e) {
                System.out.println("Error. Debes introducir " +
                        "un número entero");
                scanner.next();
            }
        }while(i < NUM_LEIDOS);

        System.out.println("Valor máximo: " + maximo);
    }

    private static int leerNumeroEntero(Scanner scanner) {
        System.out.println("Introduce un número: ");
        return scanner.nextInt();
    }

}
