package es.rhidalgo.actividad1;

import java.util.InputMismatchException;
import java.util.Scanner;

public class Actividad1 {

    private static final int NUM_LEIDOS = 6;

    public static void main(String[] args) {

        Scanner scanner = new Scanner(System.in);

        int i = 0;
        int maximo = Integer.MIN_VALUE;

        do {
            try {
                System.out.println("Introduce un número: ");
                int numero = scanner.nextInt();
                if (numero > maximo) {
                    maximo = numero;
                }
                i++;
            }catch (InputMismatchException e) {
                System.out.println("Error. Debes introducir " +
                        "un número entero");
                scanner.next();
            }
        }while(i < NUM_LEIDOS);

        System.out.println("Valor máximo: " + maximo);
    }


}
